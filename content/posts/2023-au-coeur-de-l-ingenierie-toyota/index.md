---
Categories:
    - Flow
Tags:
    - Lean
    - Enseignement
    - Lecture
date: 2023-05-27
title: Au cœur de l'ingénierie Toyota
images:
    - chasseur-philosophe-ingenieur-paysan.jpg
cover:
    description: Le chasseur-philosophe et l'ingénieur-paysan
    brightness: 60%
---

Il y a quelques mois, [Perrick](https://obeya.social/perrick) m'a proposé de lire un livre de la bibliothèque de l'[Institut Lean France](https://www.institut-lean-france.fr/). J'ai choisi [Au cœur de l'ingénierie Toyota](https://www.editions-harmattan.fr/livre-au_coeur_de_l_ingenierie_toyoto_le_chasseur_philosophe_et_l_ingenieur_paysan_cecile_roche_olivier_soulie-9782140331992-76349.html). Comme j'ai beaucoup aimé ce livre, je vous partage cette petite fiche de lecture.

**Thèmes** : Culture Lean, pour un livre de la bibliothèque de l'Institut Lean France, c'est pas très surprenant.

**L’auteur** : Olivier Soulié, sensei malgré lui, ancien "R&D Excellence System manager" chez PSA.

**Pourquoi j'ai aimé ce livre** : C'est un livre facile à lire, qui alterne des histoires et des passages plus théoriques. C'est un véritable voyage initiatique avec "Soulié-san" dans ses apprentissages et ses découvertes. J'ai eu l'impression d'apprendre avec lui, de rigoler avec lui, en bref, de vivre ces quelques années de collaborations entre franco-japonaises.

**À lire si ...** : Vous êtes dirigeant ou manager et cherchez à faire évoluer votre culture lean en vous appuyant sur des expériences concrètes.

**Quelques-uns de mes apprentissages** :

* Le rôle principal du manager c'est d'aider ses équipes à apprendre, d'accompagner ses managés dans leurs apprentissages. En un mot d'être leur sensei.
* La règle d'or du manager : faire aveuglément confiance à ses managés, ne jamais les remettre en cause, chaque erreur est en effet une incroyable source d'apprentissage. Pour le manager ou le managé.
* Quelque soit l'action, le plus important c'est la façon de travailler et l'apprentissage, pas le résultat.
* La majorité de la documentation est personnelle. C'est le fait de rédiger de la documentation qui permet d'apprendre. La connaissance commune c'est la somme de toutes les connaissances de chacun. Corollaire, il est important de faire attention à la pyramide des âges de l'entreprise pour garantir la transmission.
* Le TPS (Toyota Production System) en une phrase : tout doit rester simple, tout doit être visible, faîtes confiance à vos équipes, ils sauront faire les bonnes choses.
* Enfin je cherchais un mot français pour remplacer le traditionnel "take away" (emporter) et je l'ai trouvé : [viatique](https://fr.wiktionary.org/wiki/viatique), c'est une provision — nourriture ou somme d'argent — donnée pour un voyage.

**Niveau de difficulté** : 2/5, une première lecture très facile qui demande un petit peu de recul pour en tirer tous les apprentissages.

**Edition** : L’Harmattan

