---
date: 2022-09-23
title: "Une rétrospective à plus de 100 et en ligne"
draft: false
authors:
  - clavier_thomas
Tags:
  - rétrospective
  - distance
  - workadventure
  - klaxoon
  - rex
Categories:
  - Enseignement
  - Agilité
images:
  - islands.png
cover:
    description: Screenshot Map islands WorkAdventure
    brightness: 50%
description: |
  Faire une rétrospective à plus de 100 personnes et en ligne c'est possible. Retour d'expérience d'un défi hors norme.
---

C'était en 2021, fin mars 2021 pour être précis. Dans le cadre de la [startup week](https://startupweek.fr) je me suis retrouvé à animer une rétrospective à distance avec plus de 100 personnes. Petit retour d'expérience.

# Startup Week

J'organise chaque année une semaine hors norme pour quelques entrepreneurs du Nord et les étudiants de l'IUT de Lille, une semaine de code intensif avec du déploiement continu, des livraisons toutes les 2 heures et un grand salon de clôture.

Imaginez, une centaine d'étudiants du département informatique, une grosse vingtaine d'étudiants de GEA et entre 10 et 16 porteurs de projets. Je mélange le tout et je construis 16 équipes qui vont vivre une semaine intensive en mode startup. Chaque équipe poursuit une seul objectif : montrer une belle preuve de concept utilisable le jour du salon. Une preuve de concept qui permettra au porteur de tester un petit bout de son MVP (Produit Minimum Viable).


# Une rétrospective

En tant que bon agiliste il va de soit que cette activité est clôturée par ce que SAFe appel le rituel "Inspect and Adapt".

Cette année les conditions sanitaires ne me permettaient pas d'organiser une rétrospective en présentiel, j'ai donc opté pour une version tout en ligne.

## Les outils

Pour fédérer tout ce petit monde, j'ai opté pour deux outils en ligne :

* [WorkAdventure](https://workadventu.re/) installée et configurée sur un gros serveur loué pour l'occasion chez [OVH](https://www.ovh.com/fr/).
* Un tableau blanc [Klaxoon](https://klaxoon.com/)
* Un World Café pour organiser tout le monde.

## WorkAdventure

WorkAdventure et une solution ludique de visio conférence permettant à la fois des visios en pair à pair pour de tout petits groupes (moins de 5) et de grandes visios avec [Jitsi](https://meet.jit.si/). 

Pour l'occasion j'avais créé une carte composée de 20 petites îles autour d'une grande. L'idée de départ était d'avoir au centre une grande salle de visio pour les moments de plénières et sur chaque petite île de la périphérie, une zone de visio pour reproduire l'ambiance de la table du world café. Seulement les premiers tests ont montré un énorme défaut, on passe beaucoup trop de temps à passer d'une petite île à l'île centrale. Pour corriger ça, j'ai ajouté une petite zone de visio directement rattachée à la salle de visio principale. Donc chaque île avait 2 zones de visios, une propre à l'île et une rattachée à la conférence principale. Cette dernière ayant servi à la fois comme lieu pour demander de l'aide et comme lieu d'échange après chaque round du World Café.

## Klaxoon

J'ai partagé le grand tableau blanc en 20 colonnes et 5 lignes, 1 colonne par île et une ligne par étape du world café. Dans chaque case, les participants pouvaient retrouver les consignes spécifiques à l'étape en cours. De mon côté je pouvais voir les post-it arriver dans chaque case et identifier depuis la vue d'ensemble si l'un ou l'autre des groupes avait des difficultés.

Enfin pendant les plénières j'ai ajouté des notes de tout ce qui était raconté dans une zone à part du tableau.

## Le déroulé

Dans un vrai World Café, il est facîle de voir à chaque étape si l'une ou l'autre des tables n'est pas complète, ici, je m'attendais à ce que ce soit compliqué ... Pour contourner ce problème j'ai inventé un nouveau protocole de mélange des tables.

J'avais configuré la carte pour que tout le monde arrive sur l'île principale en début de rétrospective. Le Jitsi s'est rapidement rempli de monde. Seul les enseignants avaient une caméra active, il faut préciser que j'avais configuré la salle de visio pour ne pas activer les caméras au delà de 3 participants. J'ai alors accueilli tout le monde, rappelé l'objectif : Faire une semaine encore mieux l'année prochaine ! Puis j'ai expliqué le déroulé suivant : 

* Sur cette carte, chaque fois que vous marchez sur des fleurs vous entrez dans une visio.
* Les fleurs blanches correspondent à la visio principale.
* Les fleurs rouge correspondent à des salles privées.
* Vous avez tous reçu dans notre outil de messagerie instantanée l'île sur laquelle vous allez passer la première étape.
* En début d'étape vous allez décider d'une personne à abandonner sur l'île, ne vous inquiétez pas il sera pris en charge par l'équipe suivante.
* Vous répondez aux questions décrites dans le klaxoon, puis nous nous retrouvons dans 10 min dans la visio principale.

Chacun est parti en courant sur son île pour y raconter des histoires ou des anecdotes qui se sont déroulées durant la semaine et qui évoquaient chez eux de mauvais souvenirs. Puis nous nous sommes retrouvés en plénière. J'ai alors demandé si l'un ou l'autre avait envie de partager une histoire. Au bout de 10 minutes de partage j'ai annoncé qu'il était temps de faire la seconde étape.

* J'invite tout le monde sauf les rapporteurs (celui ou celle que vous abandonnez sur votre île) à aller sur l'île +1.
* Vous y accueillez chaleureusement celle ou celui qui a été lâchement abandonné par son équipe.
* Vous décidez d'une nouvelle personne à abandonner sur l'île en fin d'étape.
* Vous répondez aux questions décrites dans le klaxoon, puis nous nous retrouvons dans 7 min dans la visio principale. 

Chacun est alors parti sur sa nouvelle île pour construire les choses à améliorer depuis les histoire de cette nouvelle île. Puis nous nous sommes retrouvés en plénière. J'ai alors demandé à chaque équipe de me donner la chose la plus importante pour eux à améliorer qui n'avait pas déjà été évoquée par une autre équipe. Au bout de 30 minutes, en effet ils avaient tous quelque chose à raconter, j'ai annoncé qu'il était temps de passer à l'étape suivante.

Chacun est parti sur sa nouvelle île pour y raconter des histoires qui se sont déroulées durant la semaine et qui évoquaient chez eux de bons souvenirs. Puis nous nous sommes retrouvés en plénière pour partager les moments les plus savoureux, avant de repartir pour l'étape suivante.

Chacun est alors parti sur sa nouvelle île pour construire les choses à garder depuis les histoire de leur nouvelle île. Nous nous sommes à nouveau retrouvés en plénière pour partager la chose la plus importante pour eux à garder, qui n'avait pas déjà été évoquée par une autre équipe.

Les deux étapes suivantes étaient plus personnelles. Elles avaient comme objectif de les faire réfléchir aux apprentissages de la semaine, puis aux apprentissages de l'IUT. 

> * Imaginez, c'est le début de votre stage, probablement mercredi matin, que voyez vous de changé par rapport à avant le début de la start-up week ?  Qu'allez-vous faire de différent ?
> * Imaginez-vous en septembre prochain, vous avez eu votre diplôme.  Que voyez vous de changé suite à ces 2 ans d'études ?  Qu'allez-vous faire de différent ?

# Conclusion

2h30 de rétrospective qui se sont drôlement bien déroulées, avec des témoignages poignants et une grande majorité des étudiants qui sont restés actifs jusqu'au bout. Je repars avec un tableau blanc plein de cœurs, quelques axes d'améliorations actionnables et plein d'énergie pour relancer la start-up week l'année prochaine. 

Le déroulé consistant à les laisser se défouler tous les élémnents désagréables avant de se concentrer sur le positif a été bien plus efficace que ce que je faisais les années précédentes. Et les deux dernières étapes très personnelles ont permis de ritualiser la fin de leurs deux années d'études à l'IUT de façon très positive.

Enfin, si vous êtes startuper, intra-preneur ou porteur d'un projet je vous attends l'année prochaine pour jouer avec nous.

{{< figure src="./board.png" title="Les points positifs du board" width="100%">}}

