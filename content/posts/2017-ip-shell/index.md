+++
Categories = ["Craft"]
Tags = ["Development", "shell", "bash"]
date = "2017-02-27T12:41:00+02:00"
title = "IP et shell"
images = ["network-cover.jpg"]
aliases = ["/post/ip-shell", "/article/ip-shell", "/articles/ip-shell"]
[cover]
brightness = "40%"
+++

Quelques idées pour trouver son ip privée en shell

* Regarder la route par défaut d'une IP presque au hasard

```
ip route get 8.8.8.8| grep src| sed 's/.*src //g'
```

* Prendre la première des ips listé par la commande `hostname --all-ip-addresses`

```
hostname -I | cut -f 1 -d ' '
```

