---
Categories: 
    - Agilité
    - Enseignement
Tags:
    - rétrospective
    - workadventure
    - klaxoon
    - rex
    - visio
date: 2021-04-14
title: Une rétrospective à plus de 100 et en ligne
images: 
    - map-islands.png
cover:
    description: Screenshot Map islands WorkAdventure
    brightness: 50%
---

Il y a quelques jours, j'ai partagé sur [Ajiro](https://ajiro.fr/) un retour d'expérience sur une rétrospective hors norme. J'ai en effet animer une très grande rétrospective avec plus de 100 personnes à distance ...
Et chose surprenante, ça, c'est bien passé. Je vous invite à aller le lire sur [Ajiro](https://ajiro.fr/articles/2021-04-10_une-retro-en-ligne-a-plus-de-100/)
