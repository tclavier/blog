---
Categories:
    - Craft
Tags:
    - gpg
    - gnupg
    - openpgp
    - securité
    - mail
date: 2022-07-13
title: C'est quoi ce fichir .asc
images:
    - multipass.jpg
cover:
    description: multipass et Leeloo - Le cinquième élément
    brightness: 50%
---

On me demande régulièrement :

> C'est quoi cette pièce jointe que j'arrive pas à ouvrir ?

Le fichier .asc qui accompagne l'ensemble de mes mails, c'est ma signature électronique. Je pourrais revenir sur la très longue liste des raisons techniques qui font que le mail n'est pas fiable, raisons qui explique aussi pourquoi le spam est autant présent dans nos boites mail. Mais ce n'est pas l'objectif de cet article. Alors résumons, le mail n'est pas fiable du tout. 
Ajouter une signature électronique ajoute des element de fiabilité, en effet, vous avez maintenant la possibilité de vérifier 2 choses : 
- vous assurer que mon message n'a pas été modifié par un intermédiaire
- et que j'en suis bien l'auteur.

J'utilise [gpg](https://fr.wikipedia.org/wiki/GNU_Privacy_Guard) pour signer et chiffrer mes messages au standard [OpenPGP](https://fr.wikipedia.org/wiki/OpenPGP), et le format [S/MIME](https://fr.wikipedia.org/wiki/S/MIME) pour attacher ma signature à mes messages.

Microsoft propose une implémentation de [S/MIME pour office 365](https://docs.microsoft.com/en-us/Exchange/policy-and-compliance/smime/smime), si votre organisation active cette option vous allez recevoir une alerte disant que mes messages sont trop sécurisés pour votre organisation. Microsoft a en effet réussit l'exploit de faire un truc incapable de collaborer avec le standard. 

Si vous aussi vous n'avez rien a cacher et que vous souhaitez chiffrer et signer vos messages avec gpg il va falloir regarder du côté de :
- [Thunderbird](https://www.thunderbird.net/) si vous cherchez un client mail tout en un
- [gpg4win](https://www.gpg4win.org/) si vous êtes sous windows avec outlook
- [gpg suite](https://gpgtools.org/) si vous êtes sous MacOS avec apple mail
- [k9 mail](https://fr.wikipedia.org/wiki/K-9_Mail) avec [OpenKeyChain](https://www.openkeychain.org/) si sous êtes sous Android

Et la prochaine fois que nous nous croiserons dans une conference ou autour d'un café, n'oublions pas d'échanger nos clés publiques. En attendant, vous pouvez télécharger ma clé sur les serveurs [SKS](https://spider.pgpkeys.eu/) comme https://keyserver.ubuntu.com/ vous pouvez chercher l'identifiant de clé suivant : 20CCE23E53E6E41A


