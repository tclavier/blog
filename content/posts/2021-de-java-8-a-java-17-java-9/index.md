---
date: 2021-04-05
title: 7 ans d'évolutions de java - java 9
images: 
    - duke-surf.png
cover:
    description: Duke surfing on cloud
    brightness: 50%
tags:
    - java
    - software craftsmanship
aliases:
    - /post/de-java-8-a-java-17-java-9
    - /article/de-java-8-a-java-17-java-9
---

18 mars 2014, publication de Java 8. Septembre 2017 arrivée de Java 9 et lancement d'une grande nouveauté, nous aurons à partir de ce moment, une nouvelle version de Java tous les 6 mois. 

Je vous propose un retour sur les nouveautés qui ont jalonnés ces années d'évolution. Aujourd'hui, première partie, Java 9.

<!--more-->

Vous trouverez l'ensemble des exemples dans ce dépôt git : https://gitlab.com/tclavier/de-java-8-a-java-17

# Java 9

## Try with resources

En Java 9, il est possible de déclarer à l'extérieur du `try` les variables "Closeable". Par exemple, ce `reader` est fermé dans la clause "finaly", 

Avant :
```java
try (BufferedReader reader = new BufferedReader(new FileReader("fichier.txt"))) {
    System.out.println(reader.readLine());
} catch (IOException e) {
    e.printStackTrace();
}
```

Avec Java 9 :
```java
BufferedReader reader = new BufferedReader(new FileReader("fichier.txt"));

try (reader) {
    System.out.println(reader.readLine());
} catch (IOException e) {
    e.printStackTrace();
}
```

## Méthodes privées dans les interfaces

Titre autoporteur, en java 9 il est possible de mettre des méthodes privées dans les interfaces

```java
public interface InterfaceWithPrivateMethod {
    private void foo() {
        System.out.println("Do something");
    }
}
```

## Opérateur diamant dans les classes anonymes

```java
UnaryOperator<Integer> inverse = new UnaryOperator<>() {
    @Override
        public Integer apply(Integer integer) {
            return -integer;
        }
};
```

## Static method of

Depuis Java 9, de nombreuses classes ont une factory `of`

```java
List<Integer> jdk8 = Arrays.asList(1, 2, 3, 4, 5);
List<Integer> jdk9 = List.of(1, 2, 3, 4, 5);
```

## API flow

* L'interface `Flow.Publisher<T>` définit des méthodes pour produire des messages et des signaux.
* L'interface `Flow.Subscriber<T>` definit des méthodes pour recevoir ces messages et ces signaux.
* L'interface `Flow.Subscription` definit the methodes pour lier les `Publisher` et les `Subscriber`.
* L'interface `Flow.Processor<T,R>` définit des méthodes pour chainer des transformations depuis le `Publisher` jusqu'au `Subscriber`
* La classe `SubmissionPublisher<T>` est une implémentation très flexible de `Flow.Publisher<T>̀ 

Rentrer dans le détail de cette API mérite un article complet.

## VarHandle

Un truc moins lourd que l'API de relfexion pour jouer avec les attributs d'une classe

Supposons la classe suivante avec une sous-classe privée

```java
public class VarHandleExemple {
    private final VarHandle treeSizeField;
    private final Tree tree = new Tree();

    public VarHandleExemple() throws NoSuchFieldException, IllegalAccessException {
            treeSizeField = MethodHandles.lookup()
                    .in(Tree.class)
                    .findVarHandle(Tree.class, "sizeInFoot", Integer.class);
    }

    public Integer getSize() {
        return (Integer) treeSizeField.get(tree);
    }

    public void setSize(Integer size) {
        treeSizeField.set(tree, size);
    }

    private static class Tree {
        Integer sizeInFoot = 0;
    }
}
```

Les méthodes `setSize` et `getSize` permettent de mettre à jour l'attribut `sizeInFoot` de la classe privée `Tree`

```java
VarHandleExemple varHandleExemple = new VarHandleExemple();
assertEquals(0, varHandleExemple.getSize());
varHandleExemple.setSize(2);
assertEquals(2, varHandleExemple.getSize());
```

## @Deprecated

2 nouveaux attributs fort utile sur la classe `Deprecated`

* `forRemoval`, pour indiqué si l'élément déprécié va être supprimer dans un futur plus ou moins proche.
* `since`, pour indiquer epuis quand l'élément annoté est déprécié. Il est d'usage d'utiliser le numéro de version applicative.

## Des outils

Pour finir, Java 9 est arrivé avec quelues nouveaux outils.

### JShell : un REPL java.

```shell
$ jshell
|  Welcome to JShell -- Version 17-ea
|  For an introduction type: /help intro

jshell> Integer foo = Integer.parseInt("12");
foo ==> 12

jshell> 
```

### Jlink

Pour construire son propre JRE allégé avec uniquement les modules que nous souhaitons utiliser.

### Jdeprscan 

Pour scanner les classes afin de vérifier l'utilisation d'API dépréciées.


Voilà, c'est fini pour cette fois, la prochaine fois je vous parlerais de java 10.
