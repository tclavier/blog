+++
Categories = ["Craft"]
Description = "Changer la police de votre site par Luciole"
Tags = ["Luciole", "css", "font"]
date = "2024-05-14T17:31:04+02:00"
title = "Luciole"
draft = false
images = [ "head-luciole.jpg" ]
+++

Il y a quelques années, je vous parlais de la police [Linux Libertine](/2015/11/16/linux-biolinum/), une police libre permettant d'écrire quasiment dans toutes les langues, c'est d'ailleurs la police utilisée par [Wikipedia](https://fr.wikipedia.org/wiki/Linux_Libertine).

Il existe une autre police libre que j'aime beaucoup, elle a beaucoup moins de glyphes, 700 alors que Linux Libertine en contient plus de 2000. Ce qui permet d'écrire dans quasiment toutes les langues européennes. Sa particularité c'est d'avoir été concue et travaillée pour être la plus lisible possible même par des malvoyants, plusieurs études à l'appuie (au laboratoire DIPHE de l'Université Lumière Lyon 2, au [CTRDV](https://www.ctrdv.fr/), à l'ISTR Département d’Orthoptie de l'Université Lyon 1). Cette police c'est [Luciole](https://luciole-vision.com/)

# Téléchargement

La première chose à faire c'est de télécharger les fichier `ttf` correspondants aux différentes variantes de la police, il va falloir se rendre dans la section [Téléchargement du site Luciole](https://luciole-vision.com/#download)

# CSS

La seconde étape consiste à déclarer les fichiers de polices dans votre feuille de style :

```css
@font-face {
    font-family: "Luciole";
    src: url('/fonts/Luciole-Regular.ttf') format('truetype');
}

@font-face {
    font-family: "Luciole";
    font-style: italic;
    src: url('/fonts/Luciole-Regular-Italic.ttf') format('truetype');
}

@font-face {
    font-family: "Luciole";
    font-weight: bold;
    src: url('/fonts/Luciole-Bold.ttf') format('truetype');
}

@font-face {
    font-family: "Luciole";
    font-style: italic;
    font-weight: bold;
    src: url('/fonts/Luciole-Bold-Italic.ttf') format('truetype');
}

```

Nous pourrons profiter des variantes italique et grasse de la police. Il ne reste plus qu'à l'utiliser dans notre document, la feuille de style pourra par exemple avoir les lignes suivante : 

```css
body {
    font-family: Luciole, Open Sans, sans-serif;
}
```

# Conclusion

L'inclusion est un vaste sujet qui nécessite des choix et des compromis. Est-il préférable d'avoir une police pour toutes les langues ou une police lisible pour les déficients visuels ? Est-il préférable de d'avoir une police que presque tout le monde a déjà sur son poste même si elle est propriétaire ou faut-il privilégier une police libre que tout le monde va devoir télécharger ?

Pour ce blog, j'ai fait le choix d'une police libre et adaptée aux déficients visuels avec en plus quelques tests automatiques [Pa11y](https://pa11y.org/).
