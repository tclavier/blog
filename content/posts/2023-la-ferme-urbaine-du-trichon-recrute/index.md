---
Categories:
    - Craft
Tags:
    - vie de quartier
    - ferme
    - urbaine
date: 2023-01-06
title: Recruter une maraîchère ou un maraîcher à Roubaix
images:
    - ferme-trichon.jpg
cover:
    description: Maraichage à la ferme urbaine du trichon
    brightness: 50%
---
J'habite dans un quartier de Roubaix dans lequel il se passe des choses extraordinaires c'est le quartier du Trichon. Nous avons entre autre vue la création d'une ferme urbaine avec un jardin partagé. Promis je vous partagerais quelques photos de ce lieu d'accueil et d'échange fort agréable. Aujourd'hui, le projet de ferme urbaine et circulaire du Trichon recrute un ou une maraîchère en CDI, comme ils n'ont pas encore de site web, l'annonce est hébergée ici.

## L'annonce

En plein centre de Roubaix et en lien étroit avec la puissance publique, le Collectif des paysans urbains du Trichon mène depuis un an une double expérimentation:
* reconstituer un sol fertile sur une friche de 6300m² à partir de matière organique locale en vue d’installer une ferme maraîchère en auto-récolte à partir de 2025,
* suivre le comportement des polluants organiques et inorganiques présents dans une zone de 300m² et une zone témoin non polluée en coopération avec 3 laboratoires universitaires et un bureau d’étude.

L’association poursuit ainsi plusieurs objectifs:
* reconnexion au vivant,
* relocalisation du modèle alimentaire,
* accès, y compris pour les moins fortunés, au bien-vivre alimentaire,
* éducation populaire,
* amélioration du cadre de vie, résilience face au dérèglement climatique et à l'effondrement de la biodiversité.

Tes missions
* Cultiver les légumes test en coopération avec les scientifiques,
* mettre en œuvre la reconstitution du sol en lien avec le chargé de projet reconstitution,
* encadrer une apprentie,
* participer au design de la future ferme (drainage, irrigation, choix culturaux, implantation, etc.) et contribuer à l’écriture de son modèle économique en vue, le cas échéant, de s’installer ensuite sous une forme à écrire ensemble,
* contribuer à la politique d’éducation populaire de l'association en accueillant des groupes d'enfants et d'adultes pour des ateliers en lien avec la chargée de projet appropriation.

Ton contrat et tes points forts
* CDI à 35 h par semaine, à négocier, plancher bas 1750€ net (temps plein) + avantages repas sur place. Temps partiel possible.
* Capacité à travailler en équipe, à gérer l'imprévu, à s'impliquer, à coopérer, à débriefer et tirer les leçons de ce qui marche et de ce qui coince.
* Diplôme et/ou expérience justifiée en maraîchage ou gestion d’espaces naturels.

Renseignements et candidature par mail : contact chez fermeurbainedutrichon.fr

Retrouvez l'intégralité de l'annonce dans le [pdf](./2023-01-03_annonce.pdf)
